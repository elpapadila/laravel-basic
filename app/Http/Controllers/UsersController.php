<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
  function index(){
    /**
     * whereNotIn('id', [auth()->user()->id])
     * agar user id yang digunakan tidak ditampilkan
     */
    $collection = User::orderBy('id', 'desc')
    ->whereNotIn('id', [auth()->user()->id])
    ->get();

    return view('user.index', [
      'collection' => $collection
    ]);
  }

  function edit($id){
    $item = User::find($id);

    if (!$item) {
      return abort(404);
    }

    return view('user.edit', [
      'item' => $item,
      'profile' => null
    ]);
  }

  function store(Request $req){
    try {
      $validator = Validator::make($req->all(), [
        'email' => 'unique:users,email',
      ], [
        'email.unique' => "Email $req->email sudah terdaftar!",
      ]);

      if ($validator->fails()) {
        throw new InvalidArgumentException("Pemberitahuan : ".$validator->errors()->first(), 500);
      }

      $create = User::create(array_merge([
        'password' => Hash::make($req->password)
      ], $req->only([
        'first_name', 'last_name', 'birth_date', 'email'
      ])));

      return redirect('/user');

    } catch (\Throwable $th) {
      return response()->json([
        'message' => $th->getMessage()
      ], 500);
    }
  }

  function delete(Request $req){
    $find = User::find($req->id);

    if (!$find) {
      return 'User tidak ditemukan!';
    }

    if ($find->id == auth()->user()->id) {
      return 'User tidak bisa dihapus saat ini!';
    }

    $find->delete();

    return 'User berhasil dihapus!';
  }

  function update(Request $req){
    try {
      $validator = Validator::make($req->all(), [
        'email' => 'unique:users,email,'.$req->id,
        'id' => 'required'
      ], [
        'email.unique' => "Email $req->email sudah terdaftar!",
        'id.required' => "ID tidak ditemukan!",
      ]);

      if ($validator->fails()) {
        throw new InvalidArgumentException("Pemberitahuan : ".$validator->errors()->first(), 500);
      }

      $find = User::find($req->id);

      if (!$find) {
        throw new InvalidArgumentException("Data tidak ditemukan!", 500);
      }

      $pw = $req->password;
      $find->update(array_merge([
        'password' => $pw ? Hash::make($pw) : $find->password
      ], $req->only([
        'first_name', 'last_name', 'birth_date', 'email'
      ])));

      if ($req->profile) {
        return redirect('/profile');
      }

      return redirect('/user');

    } catch (\Throwable $th) {
      return response()->json([
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ], 500);
    }
  }
}
