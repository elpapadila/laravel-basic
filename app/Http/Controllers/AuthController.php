<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    function index(Request $req){
        try {
            /*untuk mencari data user berdasarkan Email */
            $find = User::where('email', $req->email)->first();


            /**untuk mengecek apa USER ditemukan atau tidak
             * jika tidak ditemukan akan muncul notifiksai dibawah
            */
            if (!$find) {
                throw new InvalidArgumentException("Akun tidak ditemukan!", 500);
              }

              /**
               * untuk mengecek kecocokan password
               * yang ada pada database
               */
              if(!Hash::check($req->password, $find->password)) {
                throw new InvalidArgumentException("Email atau Password salah!", 500);
              }

              /**
               * Auth: untuk memberi akses login 
               * sebagai web
               */

              Auth::guard('web')->login($find, $remember = true);

              /**
               * untuk otomatis berpindah halaman ke 'dashboard'
               */
              return redirect('dashboard');

        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ], 500);
    }
}

    function logout(Request $request){
        /**
         * untuk menghapus sesi login/masuk sebagai 'web'
         */
        Auth::guard('web')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
}

function loginPage(){
    /**
     * untuk memvalidasi dimana jika sudah login
     * akan diarahkan ke halaman dashboard
     */
    if (auth()->user()) {
        return redirect('dashboard');
    }

    return view('login');
}

function profile(){
    $item = User::find(auth()->user()->id);

    if (!$item) {
      return abort(404);
    }

    return view('user.edit', [
      'item' => $item,
      'profile' => true
    ]);
}
}