<?php

namespace App\View\Components\Page;

use Closure;
use App\Models\Category;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Form extends Component
{
    /**
     * Create a new component instance.
     */

     public $item;
     public $actionurl;


    public function __construct($item = null, $actionurl = null)
    {
        $this->item = $item;
        $this->actionurl = $actionurl;

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        $categories = Category::orderBy('title', 'asc')->get();

        return view('components.page.form', [
            'categories' => $categories
        ]);
    }
}
